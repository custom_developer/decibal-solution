			<!-- footer -->
				<footer class="footer">
		  
			<div class="footer-inner">
			<h3>Sign up for our newsletter</h3>
			
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.</p>
			
			
			<div class="newsletter">
				<input type="text" placeholder="Enter your email to update"><input type="submit" value="SUBMIT">
			</div>
			
			
			
			<div class="links">
				<ul>
					<li><a href="#">Android Developer</a></li>
<li><a href="#">AngularJS Developers</a></li>
<li><a href="#">C# Developers</a></li>
<li><a href="#">Email Marketing Consultants</a></li>
<li><a href="#">Facebook Marketers</a></li>
<li><a href="#">Graphic Designers</a></li>
<li><a href="#">iOS Developers</a></li>
<li><a href="#">Javascript Developers</a></li>
				</ul>
			<ul>
					<li><a href="#">Android Developer</a></li>
<li><a href="#">AngularJS Developers</a></li>
<li><a href="#">C# Developers</a></li>
<li><a href="#">Email Marketing Consultants</a></li>
<li><a href="#">Facebook Marketers</a></li>
<li><a href="#">Graphic Designers</a></li>
<li><a href="#">iOS Developers</a></li>
<li><a href="#">Javascript Developers</a></li>
				</ul>
				<ul>
					<li><a href="#">Android Developer</a></li>
<li><a href="#">AngularJS Developers</a></li>
<li><a href="#">C# Developers</a></li>
<li><a href="#">Email Marketing Consultants</a></li>
<li><a href="#">Facebook Marketers</a></li>
<li><a href="#">Graphic Designers</a></li>
<li><a href="#">iOS Developers</a></li>
<li><a href="#">Javascript Developers</a></li>
				</ul>
			</div>
			
			
		</div>
		
		
		<div class="fbot">
		      <div class="wrapper">
			     <p>Copyright © 2018 Decibal Solution Private Limited</p>
                  
                  <div class="social">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                      <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                      <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                      <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                  </div>
			</div>
		</div>
		
	</footer>
	
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
	<?php wp_footer(); ?>
</body>
</html>
