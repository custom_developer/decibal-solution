/*$(document).ready(function(){
	var height = $('header').height();
	$('.top-banner').css('margin-top', -height);
	$('.bxslider').bxSlider({
	  minSlides: 1,
	  maxSlides: 5,
	  slideWidth: 240,
	  slideMargin: 0
	});
	$('.bxslider2').bxSlider({
	  mode: 'fade',
	  captions: true
	});
	$('.content-sec ul li:nth-child(5n+5)').addClass("noborder");
	$('.tab_nav li').click(function(){
		$(this).parents('.tab_nav').find('li').removeClass('selected');
		$(this).addClass('selected');
		$(this).parents('.tab_nav').next('.tab_content_outer').find('.tab_content').hide();
		$(this).parents('.tab_nav').next('.tab_content_outer').find('.tab_content').eq($(this).index()).show();
		$(this).parents('.tab_nav').find('.activeTabLink').html($(this).parents('.tab_nav').find('li.selected a').html());
	});
});
*/
$(document).ready(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
  var height = $('header').height();
  $('.home-slider').css('margin-top', -height);
  $('.slider-content').css('margin-top', height);
});


$(function () {
		
		var filterList = {
		
			init: function () {
			
				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixItUp({
  				selectors: {
    			  target: '.portfolio',
    			  filter: '.filter'	
    		  },
    		  load: {
      		  filter: '.app, .card, .icon, .logo, .web'  
      		}     
				});								
			
			}

		};
		
		// Run the show!
		filterList.init();
		
		
	});	           



$('.quote-flexslider').owlCarousel({
    loop:true,
    nav:false ,
    items:1
});