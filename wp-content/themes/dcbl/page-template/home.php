<?php
/*
Template Name: Homepage
*/

get_header(); ?>
<section class="home-slider">
		<div class="flexslider">
			<ul class="slides">
				<li style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/slide1.jpg)">
					 
					<div class="slider-content">
						<h2>WELCOME TO OUR DEV WORLD</h2>
						<label>We Make Awesome Theme For Your Business</label><br>
						<a href="#">Get Started</a>
					</div>
				</li>
				<li style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/slide2.jpg)">
					 
					<div class="slider-content">
						<h2>WELCOME TO OUR DEV WORLD</h2>
						<label>We Make Awesome Theme For Your Business</label><br>
						<a href="#">Get Started</a>
					</div>
				</li>
			</ul>
		</div>
	</section>

	<div class="about-section">
		<div class="wrapper">
			<h2>Something About Decibal Solution</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget 
tellus tristique bibendum. Donec rutrum sed sem quis venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rut  rum sed sem quis venenatis. Donec rutrum sed sem quis venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae 
eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis. </p>
			<a class="button" href="#">Get Started</a>
		</div>
	</div>

	<div class="services-section">
		<div class="wrapper">
			<h2>Decibal Solution Services</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. <br>Donec rutrum sed sem quis venenatis.</p>
			<ul>
				<li>
					<i class="fa fa-tablet"></i>
					<div class="service-right">
						<h3>Software Development</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
				<li>
					<i class="fa fa-lemon-o"></i>
					<div class="service-right">
						<h3>Website Development</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
				<li>
					<i class="fa fa-tablet"></i>
					<div class="service-right">
						<h3>App Development</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
				<li>
					<i class="fa fa-code"></i>
					<div class="service-right">
						<h3>UI & UX</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
				<li>
					<i class="fa fa-envelope"></i>
					<div class="service-right">
						<h3>Online Marketing</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
				<li>
					<i class="fa fa-bookmark"></i>
					<div class="service-right">
						<h3>Brand Design</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum.</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div class="funfact" style="background:url(<?php echo get_template_directory_uri(); ?>/images/ba.jpg) no-repeat;">
		<div class="wrapper">
		<div class="funfact-sec">
			<ul>
				<li>
					<label>561</label>
					<i class="fa fa-thumbs-o-up"></i>
					<span>CREATIVE DESIGN</span>
				</li>
				<li>
					<label>25</label>
					<i class="fa fa-trophy"></i>
					<span>Awards Won</span>
				</li>
				<li>
					<label>236</label>
					<i class="fa fa-group"></i>
					<span>Happy Clients</span>
				</li>
				<li>
					<label>365</label>
					<i class="fa fa-coffee"></i>
					<span>Cup of Coffee</span>
				</li>
			</ul>
		</div>
		</div>
		<div class="overlay"></div>

	</div>	
	
	
    
    
    <div class="portfolio-sec">
    <div class="wrapper">
        
        <h3>LATEST PROJECT</h3>
    
        <ul id="filters" class="clearfix">
			<li><span class="filter active" data-filter=".app, .card, .icon, .logo, .web">SHOW ALL</span></li>
			<li><span class="filter" data-filter=".app">PHOTOGRAPHY</span></li>
			<li><span class="filter" data-filter=".card">BRANDING</span></li>
			<li><span class="filter" data-filter=".icon">GRAPHIC DESIGN</span></li>
 
		</ul>

		<div id="portfoliolist">
			
			 				

			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/app/1.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Visual Infography</a>
							<span class="text-category">APP</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>		
			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">						
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/web/4.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Sonor's Design</a>
							<span class="text-category">Web design</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/card/1.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Typography Company</a>
							<span class="text-category">Business card</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
						
			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/app/3.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Weatherette</a>
							<span class="text-category">APP</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/card/4.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">BMF</a>
							<span class="text-category">Business card</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
			
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/card/5.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Techlion</a>
							<span class="text-category">Business card</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
			
			 																																					
			
			<div class="portfolio app" data-cat="app">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/app/2.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Graph Plotting</a>
							<span class="text-category">APP</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>														
			
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/card/2.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">QR Quick Response</a>
							<span class="text-category">Business card</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				

			 
			<div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/icon/4.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Domino's Pizza</a>
							<span class="text-category">Icon</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>							

		 
			<div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/icon/1.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Instagram</a>
							<span class="text-category">Icon</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			
			 
			<div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/icon/2.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Scoccer</a>
							<span class="text-category">Icon</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>																																																																

			<div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">						
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/icon/5.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">3D Map</a>
							<span class="text-category">Icon</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			
			 								
			
			<div class="portfolio logo" data-cat="logo">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/logo/3.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Native Designers</a>
							<span class="text-category">Logo</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>																	

			 																											
			
			<div class="portfolio icon" data-cat="icon">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/icon/3.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Sandwich</a>
							<span class="text-category">Icon</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>																								
			
			<div class="portfolio card" data-cat="card">
				<div class="portfolio-wrapper">			
					<img src="<?php echo get_template_directory_uri(); ?>/images/portfolios/card/3.jpg" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Reality</a>
							<span class="text-category">Business card</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	

		 		
										
			
		</div>
        </div>
    </div>
	
	
<div class="testimonial-section funfact"  style="background:url(<?php echo get_template_directory_uri(); ?>/images/testimanial.gif) no-repeat;">
	<div class="wrapper">
		 
		 <ul class="quote-flexslider">
		 	<li>
		 	<p class="quote"><i class="fa fa-quote-right" aria-hidden="true"></i></p>

		 		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly 
believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem</p><br>

<p><strong>JOHN SMITH</strong> - CEO, FOUNDER</p>


		 	</li>
		 	<li>		 	<p class="quote"><i class="fa fa-quote-right" aria-hidden="true"></i></p>

		 		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly 
believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem</p><br>

<p><strong>JOHN SMITH</strong> - CEO, FOUNDER</p>


		 	</li>
		 	<li>		 	<p class="quote"><i class="fa fa-quote-right" aria-hidden="true"></i></p>

		 		<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly 
believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem</p><br>

<p><strong>JOHN SMITH</strong> - CEO, FOUNDER</p>


		 	</li>
		 </ul>

	</div>
	<div class="overlay"></div>
</div>



<div class="imgs-section">
	<div class="wrapper">
	<img src="<?php echo get_template_directory_uri(); ?>/images/imgs.png">

	</div>
</div>
	
    <div class="contact-sec">
    	<div class="wrapper">
            <div class="col">
                <h3>CONTACT INFO</h3>
                
                <ul class="con-info">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/ico1.png" alt=""> <strong>VISIT US</strong> 
F-1002 Antriksh Golf View-2, Noida-78,India, Pin-201301 </li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/ico2.png" alt=""><strong>MAIL US</strong> 
<a href="mailto:info.decibal@gmail.com">info.decibal@gmail.com</a> </li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/ico3.png" alt=""><strong>CALL US
</strong> 
+(91) 7503429170</li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/ico4.png" alt=""><strong>WORK HOUR
</strong> 
Mon - Fri: 10 Am - 7 Pm (IST)</li>
                </ul>
                
                
                
            </div>    
            <div class="col leavemsg">
                <h3>LEAVE MESSAGE</h3>
                
                
                
                <ul>
                    <li><input type="text" placeholder="First Name"></li>
                    <li><input type="text" placeholder="Last Name"></li>
                    <li><input type="text" placeholder="Email"></li>
                    <li><input type="text" placeholder="Subject"></li>
                    <li class="full"><textarea>Your Message</textarea></li>
                    <li><input type="submit" value="SEND MESSAGE"></li>
                </ul>
                
                
                
            </div>    
        
        </div>    
    </div>


<?php get_footer(); ?>