<?php
/*
Template Name: Service Page
*/

get_header(); ?>
<section class="inner-banner clearfix" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/slide1.jpg)">
		<div class="wrapper">
			<h1>Services & Portfolio</h1>
			<ul class="breadcrumb">
				<li><a href="index.html">Home</a></li>
				<li>Services & Protfolio</li>
			</ul>
		</div>
	</section>
	<section class="services-top-con">
		<div class="wrapper">
			<div class="services-top-content">
				<h2>In a user-centric world we create value earlier and more reliably.</h2>
				<p>We develop digital brand strategies, communications, products and services that matter to your target group by novelty, brand authenticity and outstanding quality for higher brand awareness, engagement, sales and loyalty.</p>
				<p>We achieve this via a strict user-centered and agile approach. Our deliverable is always a trustworthy, tangible and shareable proof that allows a shorter time to market.</p>
			</div>
		</div>
	</section>
	<div class="main-services-section">
		<div class="wrapper">
			<div class="heading-top">
				<h3>Let's make a masterpiece</h3>
				<p>Always taking our time to be well researched, we develop truly awesome experiences for companies. At our core, we believe even toilet paper companies can be cool.</p>
			</div>
			<div class="services-section">
				<ul>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/software-dev-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>Software Development</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/website-dev-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>Website Development</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/app-dev-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>App Development</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/ui-ux-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>UI & UX</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/online-marketing-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>Online Marketing</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
					<li>
						<div class="service-content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/images/brand-design-icon.png" alt=""></figure>
							<div class="service-right">
								<h3>Brand Design</h3>
								<p>Make the most out of your digital marketing efforts with comprehensive strategy.</p>
							</div>
							<div class="hover-content">
								<a href="#" class="btn read-more">Read more</a>
							</div>
						</div>
					</li>
				</ul>
		</div>
		</div>
	</div>	
    
    
<div class="portfolio-sec">
	<div class="heading-top">
	    <h3>Projects in the Lab</h3>
	    <p>Always taking our time to be well researched, we develop truly awesome experiences for companies.<br> At our core, we believe even toilet paper companies can be cool.</p>
	</div>
	<div class="projects-wrap clearfix">
		<ul>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project01.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project02.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project03.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project04.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project05.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
			<li>
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/project06.jpg" alt=""></figure>
				<div class="hover-content">
					<div class="overlay-text">
						<h4>Project Name</h4>
						<p>Always taking our time to be well researched,<br> we develop truly awesome experiences for companies</p>
						<div class="btn-row"><a href="#" class="btn">View Project</a></div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>

<div class="company-logo clearfix">
	<div class="wrapper">
		<div class="heading-top">
			<h3>We keep good company</h3>
			<p>We have been lucky enough to work with some of the best companies and people around. We've worked<br>collaboratively with other agencies, and many in a freelance role.</p>
		</div>
		<ul>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo01.png" alt=""></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo02.png" alt=""></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo03.png" alt=""></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo04.png" alt=""></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo05.png" alt=""></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/logo06.png" alt=""></li>
		</ul>
	</div>
</div>

<?php get_footer(); ?>